package com.example.nosqlexam.repository;

import com.example.nosqlexam.entity.NewsMongo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsMongoRepository extends MongoRepository<NewsMongo, String> {
}
