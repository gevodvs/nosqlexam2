package com.example.nosqlexam.repository;

import com.example.nosqlexam.entity.NewsRedis;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsRedisRepository extends CrudRepository<NewsRedis, String> {
}
