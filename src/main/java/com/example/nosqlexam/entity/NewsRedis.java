package com.example.nosqlexam.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@Data
@RedisHash("news")
//TODO use the same object as NewsMongo
public class NewsRedis {

    @Id
    private String id;
    private String name;
    private String header;
    private String payload;
    private String redactorName;

    private String createTime;
    private String approveTime;

    private String a;
    private String b;
    private String c;
    private String d;

}
