package com.example.nosqlexam.controller;

import com.example.nosqlexam.salon.NewsResponse;
import com.example.nosqlexam.salon.NewsSaveRequest;
import com.example.nosqlexam.salon.NewsSaveResponse;
import com.example.nosqlexam.service.NewsService;
import com.example.nosqlexam.service.ReplicationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/news")
@RequiredArgsConstructor
public class NewsController {

    private final NewsService salonService;
    private final ReplicationService replicationService;

    @PostMapping
    public NewsSaveResponse save(@Valid @RequestBody NewsSaveRequest request) {
        return replicationService.save(request);
    }

    @GetMapping("/{id}")
    public NewsResponse find(@PathVariable String id) {
        return salonService.findById(id);
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        salonService.delete(id);
    }


}
