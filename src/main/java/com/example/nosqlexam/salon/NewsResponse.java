package com.example.nosqlexam.salon;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewsResponse {

    private String id;
    private String name;
    private String header;
    private String payload;
    private String redactorName;

    private String createTime;
    private String approveTime;

    private String a;
    private String b;
    private String c;
    private String d;

}
